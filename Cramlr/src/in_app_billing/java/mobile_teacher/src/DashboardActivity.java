package mobile_teacher.src;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import mobile_teacher.R;
import mobile_teacher.helper.DatabaseHelper;
import mobile_teacher.helper.GoogleAnalyticsHelper;
import mobile_teacher.helper.JSONParser;
import mobile_teacher.helper.LessonAdapter;
import mobile_teacher.model.Lesson;
import mobile_teacher.util.IabHelper;
import mobile_teacher.util.IabResult;
import mobile_teacher.util.Inventory;
import mobile_teacher.util.Purchase;
import mobile_teacher.util.SkuDetails;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by blavi on 12/17/13.
 */
public class DashboardActivity extends HeaderActivity {

    private DatabaseHelper db;
    private ListView lessonsListView;
    private JSONParser parser;
    private RelativeLayout dashboardLayout;
    private LinearLayout headerLayout;
    private ArrayList<Lesson> lessons;
    private ArrayList<String> purchases;
    private GoogleAnalyticsHelper mGaHelperInstance;
    private IabHelper mHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        headerLayout = (LinearLayout) findViewById(R.id.headerLayout);
        dashboardLayout = (RelativeLayout)getLayoutInflater().inflate(R.layout.dashboard, null);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.BELOW, headerLayout.getId());
        dashboardLayout.setLayoutParams(params);
        headerLayout.addView(dashboardLayout);

        lessonsListView = (ListView) findViewById(R.id.lessonsList);
        db = DatabaseHelper.getInstance(this);

        parser = new JSONParser();

        lessons = (ArrayList) db.getLessons();
        try {
            if (lessons.size() == 0) {
                lessons = parser.getLessons(getAssets().open("lessons.json"));
                db.storeLessons(lessons);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // In-App-Billing
        final Context context = getApplicationContext();
        String base64EncodedPublicKey = context.getResources().getString(R.string.license_key);
        // get base64EncodedPublicKey
        mHelper = new IabHelper(this, base64EncodedPublicKey);

        final List<String> purchasableItems = new ArrayList<String>();

        final IabHelper.QueryInventoryFinishedListener mQueryFinishedListener = new IabHelper.QueryInventoryFinishedListener() {
            public void onQueryInventoryFinished(IabResult result, Inventory inventory) {

                if (result.isFailure()) {
                    // display lessons list without prices
                    lessonsListView.setAdapter(new LessonAdapter(context, (ArrayList)getLessonsInAlphabeticalOrder(lessons), null));
                }
                else {
                    Map<String, String> lessonsDetailsMap = new HashMap<String, String>();
                    String sku;
                    for (int i = 0; i < purchasableItems.size(); i++) {
                        sku = purchasableItems.get(i);
                        if (!inventory.hasPurchase(sku)) {
                            lessonsDetailsMap.put(sku, ((SkuDetails) inventory.getSkuDetails(sku)).getPrice());
                        }
                    }

                    purchases = (ArrayList<String>)inventory.getAllOwnedSkus();

                    // update UI with lessons (free, purchased, not purchased)
                    lessonsListView.setAdapter(new LessonAdapter(context, (ArrayList)getLessonsInAlphabeticalOrder(lessons), lessonsDetailsMap));
                }
            }
        };

        // setup connection
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    lessonsListView.setAdapter(new LessonAdapter(context, (ArrayList) getLessonsInAlphabeticalOrder(lessons), null));
                } else {
                    // Hooray, IAB is fully set up!
                    for (int i = 0; i < lessons.size(); i++) {
                        int lessonId = getResources().getIdentifier("lesson_" + String.valueOf(lessons.get(i).getId()), "string", context.getPackageName());
                        if (lessonId != 0) {
                            String sku = context.getString(lessonId);
                            purchasableItems.add(sku);
                        }
                    }

                    mHelper.queryInventoryAsync(true, purchasableItems, mQueryFinishedListener);
                }
            }
        });

        final IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
            public void onIabPurchaseFinished(IabResult result, Purchase purchase){
                if (result.isFailure()) {
                    Toast.makeText(context, result.getMessage(), Toast.LENGTH_LONG).show();
                    Log.d("Failure", "Error purchasing: " + result);
                    return;
                } else {
                    mHelper.queryInventoryAsync(true, purchasableItems, mQueryFinishedListener);
                }
            }
        };

        // handle list item click
        final Activity activity = this;
        lessonsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Lesson lesson = ((Lesson) parent.getItemAtPosition(position));
                int lessonId = getResources().getIdentifier("lesson_" + String.valueOf(lesson.getId()), "string", context.getPackageName());

                if (lessonId != 0){
                    String sku = context.getString(lessonId);

                    if (purchases == null) {
                        Toast.makeText(context, "No Internet connection", Toast.LENGTH_LONG).show();
                    } else if (!purchases.contains(sku)) {
                        try {
                            mHelper.launchPurchaseFlow(activity, sku, lesson.getId(), mPurchaseFinishedListener, "");
                        } catch (Exception e) {

                        }
                    } else {
                        startLesson(lesson);
                    }

                } else {
                    startLesson(lesson);
                }
            }
        });

        // Get the GoogleAnalyticsHelper singleton.
        mGaHelperInstance = GoogleAnalyticsHelper.getInstance();
    }



    public void startLesson(Lesson lesson) {
        Intent i = new Intent(DashboardActivity.this, StartLessonActivity.class);

        i.putExtra("title", lesson.getTitle());
        i.putExtra("testingPercentage", String.valueOf(lesson.getTestingProgress()));
        i.putExtra("learningPercentage", String.valueOf(lesson.getLearningProgress()));
        i.putExtra("id", String.valueOf(lesson.getId()));

        DashboardActivity.this.startActivity(i);
    }

    public void onStart(){
        super.onStart();
        mGaHelperInstance.specificTrack("lessons", this);
    }

    public List<Lesson> getLessonsInAlphabeticalOrder(List<Lesson> lessons) {
        Collections.sort(lessons, new CompletedStateComparator());

        return lessons;
    }

    public class CompletedStateComparator implements Comparator<Lesson> {
        @Override
        public int compare(Lesson l1, Lesson l2) {
            return l1.getTitle().compareTo(l2.getTitle());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null)
            mHelper.dispose();
        mHelper = null;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("ActivityResult", "onActivityResult(" + requestCode + "," + resultCode + "," + data);

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }
        else {
            Log.d("ActivityResult", "onActivityResult handled by IABUtil.");
        }
    }
}
