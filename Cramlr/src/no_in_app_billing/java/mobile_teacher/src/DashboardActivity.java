package mobile_teacher.src;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import mobile_teacher.R;
import mobile_teacher.helper.DatabaseHelper;
import mobile_teacher.helper.GoogleAnalyticsHelper;
import mobile_teacher.helper.JSONParser;
import mobile_teacher.helper.LessonAdapter;
import mobile_teacher.model.Lesson;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by blavi on 12/17/13.
 */
public class DashboardActivity extends HeaderActivity {

    private DatabaseHelper db;
    private ListView lessonsListView;
    private JSONParser parser;
    private RelativeLayout dashboardLayout;
    private LinearLayout headerLayout;
    private ArrayList<Lesson> lessons;
    private GoogleAnalyticsHelper mGaHelperInstance;
    private Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        headerLayout = (LinearLayout) findViewById(R.id.headerLayout);
        dashboardLayout = (RelativeLayout)getLayoutInflater().inflate(R.layout.dashboard, null);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.BELOW, headerLayout.getId());
        dashboardLayout.setLayoutParams(params);
        headerLayout.addView(dashboardLayout);

        lessonsListView = (ListView) findViewById(R.id.lessonsList);
        db = DatabaseHelper.getInstance(this);

        parser = new JSONParser();

        lessons = (ArrayList) db.getLessons();
        try {
            if (lessons.size() == 0) {
                lessons = parser.getLessons(getAssets().open("lessons.json"));
                db.storeLessons(lessons);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        lessonsListView.setAdapter(new LessonAdapter(this, (ArrayList)getLessonsInAlphabeticalOrder(lessons)));

        context = getApplicationContext();

        lessonsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Lesson lesson = ((Lesson)parent.getItemAtPosition(position));

                boolean lock = PreferenceManager.getDefaultSharedPreferences(DashboardActivity.this).getBoolean(String.valueOf(lesson.getId()), false);
                int resId = getResources().getIdentifier("pass_lesson_" + String.valueOf(lesson.getId()), "string", context.getPackageName());
                if (resId != 0){
                    String pass = getString(resId);
                    if (pass != "" && lock == false){
                        showPassDialog(pass, lesson);
                    } else {
                        startLesson(lesson);
                    }
                } else {
                    startLesson(lesson);
                }
            }
        });

        // Get the GoogleAnalyticsHelper singleton.
        mGaHelperInstance = GoogleAnalyticsHelper.getInstance();
    }

    public void startLesson(Lesson lesson) {
        Intent i = new Intent(DashboardActivity.this, StartLessonActivity.class);

        i.putExtra("title", lesson.getTitle());
        i.putExtra("testingPercentage", String.valueOf(lesson.getTestingProgress()));
        i.putExtra("learningPercentage", String.valueOf(lesson.getLearningProgress()));
        i.putExtra("id", String.valueOf(lesson.getId()));

        DashboardActivity.this.startActivity(i);
    }

    public void savePreferences(int lessonId) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean(String.valueOf(lessonId), true);

        editor.commit();

    }

    public void showPassDialog(String pass, final Lesson lesson) {
        View promptsView = LayoutInflater.from(context).inflate(R.layout.pass_prompt, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DashboardActivity.this);
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.user_input);

        final String password = pass;

        // set dialog message
        alertDialogBuilder
                .setTitle("Enter password to proceed:")
                .setCancelable(false)
                .setNegativeButton("Go",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                /** DO THE METHOD HERE WHEN PROCEED IS CLICKED*/
                                String user_text = (userInput.getText()).toString();

                                /** CHECK FOR USER'S INPUT **/
                                if (user_text.equals(password)) {
                                    Log.d(user_text, "HELLO THIS IS THE MESSAGE CAUGHT :)");
                                    savePreferences(lesson.getId());
                                    startLesson(lesson);
                                } else {
                                    Log.d(user_text, "string is empty");
                                    String message = "The password you have entered is incorrect.";
                                    AlertDialog.Builder builder = new AlertDialog.Builder(DashboardActivity.this);
                                    builder.setTitle("Error");
                                    builder.setMessage(message);
                                    builder.setPositiveButton("Cancel", null);
                                    builder.create().show();

                                }
                            }
                        }
                )
                .setPositiveButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }

                        }
                );


        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void onStart(){
        super.onStart();
        mGaHelperInstance.specificTrack("lessons", this);
    }

    public List<Lesson> getLessonsInAlphabeticalOrder(List<Lesson> lessons) {
        Collections.sort(lessons, new CompletedStateComparator());

        return lessons;
    }

    public class CompletedStateComparator implements Comparator<Lesson> {
        @Override
        public int compare(Lesson l1, Lesson l2) {
            return l1.getTitle().compareTo(l2.getTitle());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
