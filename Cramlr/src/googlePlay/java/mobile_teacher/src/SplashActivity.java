package mobile_teacher.src;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;

import mobile_teacher.R;

import java.util.Timer;
import java.util.TimerTask;

import mobile_teacher.helper.GoogleAnalyticsHelper;

/**
 * Created by blavi on 3/4/14.
 */
public class SplashActivity extends Activity {

    private static final long DELAY = 500;
    private boolean scheduled = false;
    private static boolean licensed = true;
    private static boolean didCheck = false;
    private static boolean checkingLicense = false;
    private Handler mHandler;
    private Timer splashTimer;
    private GoogleAnalyticsHelper mGaHelperInstance;
    private com.google.android.vending.licensing.LicenseCheckerCallback mLicenseCheckerCallback;
    private com.google.android.vending.licensing.LicenseChecker mChecker;
    private String base64_public_key;
    private static final byte[] SALT = new byte[] {
            7, 112,	-6,	-41, 77,
            -11, -7, -120, -76,	-32,
            -49, -34, 23, 120, 13,
            -107, 37, -45, 5, 112
    };

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splash);

        // Get the GoogleAnalyticsHelper singleton.
        mGaHelperInstance = GoogleAnalyticsHelper.getInstance();


        // Check the license
        checkLicense();
    }

    public void onStart(){
        super.onStart();
        Resources appR = this.getResources();
        CharSequence appName = appR.getText(appR.getIdentifier("app_name",
                "string", this.getPackageName()));

        mGaHelperInstance.generalTrack(String.valueOf(appName), this);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (scheduled)
            if (splashTimer != null)
                splashTimer.cancel();
        if (splashTimer != null)
            splashTimer.purge();
        mChecker.onDestroy();
    }

    private void displayResult(final String result) {
        mHandler.post(new Runnable() {
            public void run() {

                setProgressBarIndeterminateVisibility(false);

            }
        });
    }

    protected void doCheck() {

        didCheck = false;
        checkingLicense = true;
        setProgressBarIndeterminateVisibility(true);

        mChecker.checkAccess(mLicenseCheckerCallback);
    }

    protected void checkLicense() {

        base64_public_key = this.getResources().getString(R.string.license_key);

        Log.i("LICENSE", "checkLicense");
        mHandler = new Handler();

        // Try to use more data here. ANDROID_ID is a single point of attack.
        String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        // Library calls this when it's done.
        mLicenseCheckerCallback = new MyLicenseCheckerCallback();
        // Construct the LicenseChecker with a policy.
        mChecker = new com.google.android.vending.licensing.LicenseChecker(
                this, new com.google.android.vending.licensing.ServerManagedPolicy(this,
                new com.google.android.vending.licensing.AESObfuscator(SALT, getPackageName(), deviceId)),
                base64_public_key);

        doCheck();
    }

    protected class MyLicenseCheckerCallback implements com.google.android.vending.licensing.LicenseCheckerCallback {

        @Override
        public void allow(int reason) {
            Log.i("LICENSE", "allow");
            if (isFinishing()) {
                // Don't update UI if Activity is finishing.
                return;
            }
            // Should allow user access.
            displayResult(getResources().getString(R.string.allow));
            licensed = true;
            checkingLicense = false;
            didCheck = true;

            splashTimer = new Timer();
            splashTimer.schedule(new TimerTask()
            {
                @Override
                public void run()
                {
                    SplashActivity.this.finish();
                    startActivity(new Intent(SplashActivity.this, DashboardActivity.class));
                }
            }, DELAY);
            scheduled = true;
        }

        @Override
        public void dontAllow(int reason) {
            Log.i("LICENSE", "dontAllow");
            if (isFinishing()) {
                // Don't update UI if Activity is finishing.
                return;
            }
            displayResult(getString(R.string.dont_allow));
            licensed = false;
            // Should not allow access. In most cases, the app should assume
            // the user has access unless it encounters this. If it does,
            // the app should inform the user of their unlicensed ways
            // and then either shut down the app or limit the user to a
            // restricted set of features.
            // In this example, we show a dialog that takes the user to Market.
            checkingLicense = false;
            didCheck = true;

            showDialog(0);
        }

        @Override
        public void applicationError(int errorCode) {
            Log.i("LICENSE", "error: " + errorCode);
            if (isFinishing()) {
                // Don't update UI if Activity is finishing.
                return;
            }
            licensed = false;
            // This is a polite way of saying the developer made a mistake
            // while setting up or calling the license checker library.
            // Please examine the error code and fix the error.
            String result = String.format(getString(R.string.application_error), errorCode);
            checkingLicense = false;
            didCheck = true;

            //displayResult(result);
            showDialog(0);
        }
    }

    protected Dialog onCreateDialog(int id) {
        // We have only one dialog.
        return new AlertDialog.Builder(this)
                .setTitle(R.string.unlicensed_dialog_title)
                .setMessage(R.string.unlicensed_dialog_body)
                .setPositiveButton(R.string.buy_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(
                                "http://market.android.com/details?id=" + getPackageName()));
                        startActivity(marketIntent);
                        finish();
                    }
                })
                .setNegativeButton(R.string.quit_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })

                .setCancelable(false)
                .setOnKeyListener(new DialogInterface.OnKeyListener(){
                    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                        Log.i("License", "Key Listener");
                        finish();
                        return true;
                    }
                })
                .create();
    }
}
