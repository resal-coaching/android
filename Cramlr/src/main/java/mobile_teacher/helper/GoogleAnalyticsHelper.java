package mobile_teacher.helper;

import android.content.Context;
import android.content.pm.ApplicationInfo;

import mobile_teacher.R;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

/**
 * Created by blavi on 3/12/14.
 */
public class GoogleAnalyticsHelper {

    private static GoogleAnalyticsHelper googleAnalyticsHelperInstance = null;

    private GoogleAnalyticsHelper(){

    }

    public static GoogleAnalyticsHelper getInstance(){
        if (googleAnalyticsHelperInstance == null) {
            googleAnalyticsHelperInstance = new GoogleAnalyticsHelper();
        }
        return googleAnalyticsHelperInstance;
    }

    private GoogleAnalytics getGoogleAnalyticsInstance(Context mContext){
        return GoogleAnalytics.getInstance(mContext);
    }

    private boolean getBuildMode(Context mContext) {
        boolean isDebuggable = (0 != (mContext.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE));

        return isDebuggable;
    }

    private Tracker getGeneralTracker(Context mContext) {
        if (getBuildMode(mContext))
            return getGoogleAnalyticsInstance(mContext).getTracker(mContext.getResources().getString(R.string.testing_general_tracking_id));
        else
            return getGoogleAnalyticsInstance(mContext).getTracker(mContext.getResources().getString(R.string.release_general_tracking_id));
    }

    private Tracker getSpecificTracker(Context mContext) {
        if (getBuildMode(mContext))
            return getGoogleAnalyticsInstance(mContext).getTracker(mContext.getResources().getString(R.string.testing_specific_tracking_id));
        else
            return getGoogleAnalyticsInstance(mContext).getTracker(mContext.getResources().getString(R.string.release_specific_tracking_id));

    }

    public void generalTrack(String data, Context mContext) {
        Tracker tracker = this.getGeneralTracker(mContext);
        tracker.set(Fields.SCREEN_NAME, data);
        tracker.send(MapBuilder
                .createAppView()
                .build());
    }

    public void specificTrack(String data, Context mContext) {
        Tracker tracker = this.getSpecificTracker(mContext);
        tracker.set(Fields.SCREEN_NAME, data);
        tracker.send(MapBuilder
                .createAppView()
                .build());
    }
}
