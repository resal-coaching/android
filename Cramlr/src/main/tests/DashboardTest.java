package main.tests;

import android.content.Intent;
import android.test.ActivityUnitTestCase;

import mobile_teacher.R;
import mobile_teacher.src.DashboardActivity;

/**
 * Created by blavi on 6/2/14.
 */
public class DashboardTest extends ActivityUnitTestCase<DashboardActivity> {

    public DashboardActivity activity;

    public DashboardTest() {
        super(DashboardActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        Intent intent = new Intent(getInstrumentation().getTargetContext(),
                DashboardActivity.class);
        startActivity(intent, null, null);
        activity = getActivity();
    }

    public void testLayout(){
        assertNotNull(activity.findViewById(R.id.lessonsList));
    }
}
